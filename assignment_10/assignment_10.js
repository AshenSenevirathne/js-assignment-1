/* 
* Convert String to number array
*/
const GetStringAsArray = (str) => {
    return str.split("").map(num => +num);
}

/* 
* Function for get the number of moves
*/
const MaximalSquare = (strArr) => {

    let rowCount = strArr.length, maxWidth = 0;

    // Crate a matrix from inputted string array 
    let matrix = new Array(rowCount);

    for (let i = 0; i < rowCount; i++) {
        matrix[i] = GetStringAsArray(strArr[i]);
    }

    let colCount = matrix[0].length;

    // Create a matrix for dynamic programming
    let dp = new Array(rowCount + 1);
    for (let i = 0; i <= rowCount; i++) {
        dp[i] = [];
        for (let j = 0; j <= colCount; j++) {
            dp[i][j] = 0;
        }
    }

    // Find the maximum width
    for (let i = 1; i <= rowCount; i++) {
        for (let j = 1; j <= colCount; j++) {
            if (matrix[i - 1][j - 1] === 1) {
                dp[i][j] = 1 + Math.min(dp[i][j-1], dp[i-1][j-1], dp[i-1][j]);
                if(maxWidth < dp[i][j]){
                    maxWidth = dp[i][j];
                }
            }
        }
    }

    return maxWidth * maxWidth;
}

/* 
* ChessboardTraveling function caller
*/
const main = () => {
    let allInputs = [
        //["10100", "10111", "11111", "10010"],
        ["0111", "1111", "1111", "1111"],
        ["0111", "1101", "0111"]
    ];

    allInputs.forEach(strArr => {
        console.log(`Input: "${strArr}"`);
        console.log(`Output: ${MaximalSquare(strArr)}\n`);
    });

}

main();
