/* 
* Function for extract values from the string
*/
const ExtractValuesFromStr = (str) => {
    return str.split("d").map(val => +val);
}

/* 
* Function for get dies roll count
*/
const GenerateRandomNumber = (max) => {
    return Math.floor(Math.random() * (max - 1 + 1)) + 1;
}

/* 
* Function for get dies roll count
*/
const RoolDies = (str) => {

    let [count, sides] = ExtractValuesFromStr(str);
    let sum = 0, output = "";

    for (let i = 0; i < count; i++) {
        let randomNum = GenerateRandomNumber(sides);
        sum += randomNum;
        output += `${randomNum} `
    }

    console.log(`${sum}: ${output}`);
}

/* 
* RoolDies function caller
*/
const main = () => {
    let allInputs = [
        '5d12',
        '6d4',
        '1d2',
        '1d8',
        '3d6',
        '4d20',
        '100d100',
    ];

    allInputs.forEach(str => {
        RoolDies(str)
    });

}

main();
