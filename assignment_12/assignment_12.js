/* 
* Constants for words
*/
const belowTwenty = [
    '', 'one', 'two', 'three', 'four',
    'five', 'six', 'seven', 'eight', 'nine',
    'ten', 'eleven', 'twelve', 'thirteen', 'fourteen',
    'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
];

const tens = [
    '', '', 'twenty', 'thirty', 'forty',
    'fifty', 'sixty', 'seventy', 'eighty', 'ninety'
];

/* 
* Check last digit is equal to zero
*/
const CheckIsZero = (num) => {
    return (num == 0) ? "" : belowTwenty[num];
}

const HandleTwoDigits = (num) => {
    if (num < 20) {
        return belowTwenty[num];
    } else {
        return `${tens[+num.toString().charAt(0)]}${CheckIsZero(+num.toString().charAt(1))}`
    }
}

/* 
* Return word for the amount 
*/
const GetWord = (num) => {
    if (num < 100) {
        return HandleTwoDigits(num)
    }
    else {
        return `${belowTwenty[+num.toString().charAt(0)]} hundred ${HandleTwoDigits(+num.toString().substring(1))}`
    }
}

/* 
* Function for convert amount into word
*/
const AmountToWord = (amount) => {

    let [cents, firstThreeDgt, scndThreeDgt] = ["", "", ""];

    let stringAmount = amount.toString();
    let amountLength = stringAmount.length;

    // Check the cents values
    if(stringAmount.includes(".")){
        let index = stringAmount.indexOf('.');
        cents = HandleTwoDigits(stringAmount.substring(index + 1));
        amountLength = stringAmount.substring(0, index).length;
        amount  = stringAmount.substring(0, index);
    }else{
        cents = "zero";
    }

    // Genarate word for last 3 digits of the amount
    if(0 < amount){
        if(amount>= 1000){
            scndThreeDgt = GetWord(+amount.toString().substring(amountLength-3));
        }else{
            scndThreeDgt = GetWord(+amount.toString().substring(0, 2)); 
        }
        
    }

    // Genarate word for first 3 digits of the amount
    if(1000 < amount){
        firstThreeDgt = GetWord(+amount.toString().substring(0,amountLength-3));
    }

    let output =  `${firstThreeDgt} ${(firstThreeDgt !== "")?"thousand":""} ${scndThreeDgt} dollars and ${cents} cents.`;

    // Capitalize the first word and return the output
    return output.charAt(0).toUpperCase() + output.slice(1);

}

/* 
* AmountToWord function caller
*/
const main = () => {
    let allInputs = [
        400120.0,
        400120.00,
        400120,
    ];

    allInputs.forEach(amount => {
        console.log(AmountToWord(amount));
    });

}

main();
