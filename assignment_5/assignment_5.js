/* 
* Function for sort a string 
*/
const AlphabetSoup = (str) => {

    // Variable declaration and Intialization
    let sortedString = "";

    // Create array from string
    let strAsArray = str.split("");

    // Sort the created array
    strAsArray.sort();

    // Concat the sorted array items in to string
    strAsArray.forEach(c => sortedString += c);

    return sortedString;
}

/* 
* AlphabetSoup function caller
*/
const main = () => {
    let sortedString = AlphabetSoup("hello");
    console.log(sortedString);
}

main();
