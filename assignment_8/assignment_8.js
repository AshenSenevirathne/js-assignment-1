/* 
* Convert String to number array
*/
const GetStringAsArray = (str) => {
    let length = str.length;
    return str.substring(1, length-1).split(", ").map(num => +num);
}

/* 
* Format Output String
*/
const FormatOutputString = (side, val1, val2) => {
    let output;

    if (side === "LEFT"){
        output = `Left: ${val1} | Right: 0`;
    }else if(side === "RIGHT"){
        output = `Left: 0 | Right: ${val1}`;
    }else{
        output = `Left: ${val1} | Right: ${val2}`;
    }

    return output;
 }

 /* 
* Get number Diff as possitive value
*/
const GetDiff = (w1, w2) => {
    return Math.abs(w1 - w2);
}

 /* 
* Get weights as a map
*/
const GetWeightsMap = (weights) => {
    const weightsMap = new Map();

    weights.forEach(w => {
      if(weightsMap.get(w)){
        weightsMap.set(weight, weightsMap.get(weight) + 1);
      }else{
        weightsMap.set(w, 1);
      }
    });

    return weightsMap;
  };

/* 
* Function for sort a string 
*/
const ScaleBalancing = (strArr) => {

    // Variable declaration and Intialization
    let [left, right] = GetStringAsArray(strArr[0]);
    let  weights = GetStringAsArray(strArr[1]).sort();

    // Check scale is balanced
    if(left === right){
        return "Equals";
    }

    let small = (left < right) ? "LEFT" : "RIGHT";
    let diff = GetDiff(left, right);

    // Check balance by adding one weight to one side
    if(weights.includes(diff)){
        return FormatOutputString(small, diff, null);
    }

    // Check balance by adding two weight to one side
    for(let i = 0; i < weights.length-1 ; i++){
        if (weights[i] + weights[i+1] === diff){
            return FormatOutputString(small, `${weights[i]},${weights[i+1]}`, null);
        }
    }

    for(let i = 0; i < weights.length ; i++) {
        let weightsMap = GetWeightsMap(weights);
        weightsMap.set(weights[i], weightsMap.get(weights[i])-1);

        let diffAfterAddVal;

        // Check balance by adding weight first into left side
        diffAfterAddVal = GetDiff(left + weights[i], right);
        if(weightsMap.get(diffAfterAddVal) > 0){
            return FormatOutputString("BOTH", weights[i], diffAfterAddVal); 
        }

        // Check balance by adding weight first into right side
        diffAfterAddVal = GetDiff(right + weights[i], left);
        if(weightsMap.get(diffAfterAddVal) > 0){
            return FormatOutputString("BOTH", diffAfterAddVal, weights[i]); 
        }
    }

    return "not possible";
}

/* 
* ScaleBalancing function caller
*/
const main = () => {
    let allInputs = [
        // ["[5, 9]", "[1, 2, 6, 7]"],
        ["[3, 4]", "[1, 2, 7, 7]"],
        ["[13, 4]", "[1, 2, 3, 6, 14]"],
        ["[5, 5]", "[1, 2, 3]"]
    ];

    allInputs.forEach(arr => {
        console.log(`Input: "${arr[0]}", "${arr[1]}"`);
        console.log(`Output: "${ScaleBalancing(arr)}"\n`);
    });

}

main();
