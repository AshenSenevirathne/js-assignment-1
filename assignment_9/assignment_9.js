/* 
* Function for extract values from the string
*/
const ExtractValuesFromStr = (str) => {
    let length = str.length;
    let valuesArray = [];
    str.substring(1, length - 1).split(")(").forEach(subStr => { subStr.split(" ").forEach(val => valuesArray.push(+val)) });
    return valuesArray;
}

/* 
* Function for get the number of moves
*/
const ChessboardTraveling = (str) => {

    let [x, y, a, b] = ExtractValuesFromStr(str);

    return (a - x) + (b - y);
}

/* 
* ChessboardTraveling function caller
*/
const main = () => {
    let allInputs = [
        // "(1 1)(2 2)",
        "(1 1)(3 3)",
        "(2 2)(4 3)"
    ];

    allInputs.forEach(str => {
        console.log(`Input: "${str}"`);
        console.log(`Output: ${ChessboardTraveling(str)}\n`);
    });

}

main();
